<?php

/**
 * @file
 * Administrative forms for visualize
 *
 * @author Mathew Winstone <mwinstone@coldfrontlabs.ca>
 */

/**
 * Configuration form for visualize option sets
 */
function visualize_form_optionset_edit($form, &$form_state) {
  if (empty($form_state['optionset'])) {
    $optionset = visualize_optionset_create();
  }
  else {
    $optionset = $form_state['optionset'];
  }

  // Title
  $form['optionset_title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
    '#default_value' => $optionset->optionset_title,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => '255',
    '#machine_name' => array(
      'source' => array('optionset_title'),
      'exists' => 'visualize_optionset_exists',
    ),
    '#required' => TRUE,
    '#default_value' => $optionset->name,
  );

  // Options Vertical Tab Group table
  $form['options'] = array(
    '#type' => 'vertical_tabs',
  );

  $default_options = visualize_option_elements($optionset->options);
  // Add the options to the vertical tabs section
  foreach($default_options as $key => $value) {
    $form['options'][$key] = $value;
  }

  return $form;
}

/**
 * Defines the form elements use to edit the optionset
 *
 * @param array $options [optional]
 *  Pass in a set of default values for the options
 * @return array
 *  Returns the option form elements array
 */
function visualize_option_elements($options = array()) {
  // @todo
  $form = array();

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#multiple' => FALSE,
    '#description' => t('Chart type'),
    '#options' => array(
      'bar' => t('Bar'),
      'area' => t('Area'),
      'pie' => t('Pie'),
      'line' => t('Line'),
    ),
    '#default_value' => isset($options['type']) ? $options['type'] : _visualize_optionset_defaults('type'),
  );

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('Width of the chart. Leave blank to use the width of the table.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#default_value' => isset($options['width']) ? $options['width'] : _visualize_optionset_defaults('width'),
    '#element_validate' => array('_visualize_validate_number_or_empty'),
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('Height of the chart. Leave blank to use the height of the table.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#default_value' => isset($options['height']) ? $options['height'] : _visualize_optionset_defaults('height'),
    '#element_validate' => array('_visualize_validate_number_or_empty'),
  );

  $form['appendTitle'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append Title'),
    '#description' => t('Add title to chart.'),
    '#default_value' => isset($options['appendTitle']) ? $options['appendTitle'] : _visualize_optionset_defaults('appendTitle'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title for chart. Defaults to text of table\'s Caption element.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($options['title']) ? $options['title'] : _visualize_optionset_defaults('title'),
  );

  $form['appendKey'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append Key'),
    '#description' => t('Add a color key to the chart.'),
    '#default_value' => isset($options['appendKey']) ? $options['appendKey'] : _visualize_optionset_defaults('appendKey'),
  );

  $form['rowFilter'] = array(
    '#type' => 'textfield',
    '#title' => t('Row Filter'),
    '#description' => t('Selector to filter rows.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($options['rowFilter']) ? $options['rowFilter'] : _visualize_optionset_defaults('rowFilter'),
  );

  $form['colFilter'] = array(
    '#type' => 'textfield',
    '#title' => t('Column Filter'),
    '#description' => t('Selector to filter columns'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($options['colFilter']) ? $options['colFilter'] : _visualize_optionset_defaults('colFilter'),
  );

  $form['colors'] = array(
    '#type' => 'textarea',
    '#title' => t('Colors'),
    '#description' => t('A set of hex color values, used in order of appearance. Limit one value per line or separated by commas.'),
    '#cols' => 10,
    '#rows' => 8,
    '#element_validate' => array('_visualize_validate_hex_or_empty'),
    '#default_value' => isset($options['colors']) ? $options['colors'] : _visualize_optionset_defaults('colors'),
    '#value_callback' => '_visualize_color_value_processor'
  );

  $form['textColors'] = array(
    '#type' => 'textarea',
    '#title' => t('Text Colors'),
    '#description' => t('A set of hex color values, used in order of appearance. Limit one value per line or separated by commas.'),
    '#cols' => 10,
    '#rows' => 8,
    '#value_callback' => '_visualize_color_value_processor',
    '#default_value' => isset($options['textColors']) ? $options['textColors'] : _visualize_optionset_defaults('textColors'),
  );

  $form['parseDirection'] = array(
    '#type' => 'radios',
    '#title' => t('Parse Direction'),
    '#description' => t('Direction to parse the table data.'),
    '#options' => array(
      'x' => t('x-axis'),
      'y' => t('y-axis'),
    ),
    '#default_value' => isset($options['parseDirection']) ? $options['parseDirection'] : _visualize_optionset_defaults('parseDirection'),
  );

  $form['pieMargin'] = array(
    '#type' => 'textfield',
    '#title' => t('Pie Margin'),
    '#description' => t('Space around outer circle of Pie chart.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#element_validate' => array('_visualize_validate_number_or_empty'),
    '#default_value' => isset($options['pieMargin']) ? $options['pieMargin'] : _visualize_optionset_defaults('pieMargin'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'pie'),
      ),
    ),
  );

  $form['pieLabelPos'] = array(
    '#type' => 'radios',
    '#title' => t('Pie Label Position'),
    '#description' => t('The description appears usually below the radio buttons.'),
    '#options' => array(
      'inside' => t('Inside'),
      'outside' => t('Outside'),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'pie'),
      ),
    ),
    '#default_value' => isset($options['pieLabelPos']) ? $options['pieLabelPos'] : _visualize_optionset_defaults('pieLabelPos'),
  );

  $form['lineWeight'] = array(
    '#type' => 'textfield',
    '#title' => t('Line Weight'),
    '#description' => t('Stroke weight for lines in line and area charts.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#element_validate' => array('_visualize_validate_number_or_empty'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'line'),
      ),
    ),
    '#default_value' => isset($options['lineWeight']) ? $options['lineWeight'] : _visualize_optionset_defaults('lineWeight'),
  );

  $form['barGroupMargin'] = array(
    '#type' => 'textfield',
    '#title' => t('Bar Group Margin'),
    '#description' => t('Space around each group of bars in a bar chart.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#element_validate' => array('_visualize_validate_number_or_empty'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'bar'),
      ),
    ),
    '#default_value' => isset($options['barGroupMargin']) ? $options['barGroupMargin'] : _visualize_optionset_defaults('barGroupMargin'),
  );

  $form['barMargin'] = array(
    '#type' => 'textfield',
    '#title' => t('Bar Margin'),
    '#description' => t('Creates space around bars in bar chart (added to both sides of each bar).'),
    '#size' => 10,
    '#maxlength' => 255,
    '#element_validate' => array('_visualize_validate_number_or_empty'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'bar'),
      ),
    ),
    '#default_value' => isset($options['barMargin']) ? $options['barMargin'] : _visualize_optionset_defaults('barMargin'),
  );

  $form['yLabelInterval'] = array(
    '#type' => 'textfield',
    '#title' => t('Y Label Interval'),
    '#description' => t('Distance between y labels.'),
    '#size' => 10,
    '#maxlength' => 255,
    '#element_validate' => array('_visualize_validate_number_or_empty'),
    '#default_value' => isset($options['yLabelInterval']) ? $options['yLabelInterval'] : _visualize_optionset_defaults('yLabelInterval'),
  );

  return $form;
}

/**
 * Validate a form element that should have a number as value.
 */
function _visualize_validate_number_or_empty($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && !is_numeric($value)) {
    form_error($element, t('%name must be a number.', array('%name' => $element['#option_name'])));
  }
}

/**
 * Validate a form element that should be a hex value or empty
 */
function _visualize_validate_hex_or_empty($element, &$form_state) {
  foreach ($element['#value'] as $value) {
    if (!preg_match('/^#[a-f0-9]{6}$/i', $value)) {
      form_error($element, t('Values must be in hex color codes prefixed with a "#"'));
    }
  }
}

/**
 * Value process callback for colors
 */
function _visualize_color_value_processor($element, $input = FALSE, $form_state) {
  if ($input === FALSE) {
    $value = implode("\r\n", $element['#default_value']);
    return $value;
  }
  else {
    // Handle the case where both commas and line breaks are used
    $lines = explode("\r\n", $input);
    $lines = array_map('trim', $lines);
    // Define return value container
    $value = array();
    foreach ($lines as $line) {
      $value = array_merge($value, explode(',', $line));
    }
    // One last trim to remove any stray spaces or empty values
    $value = array_map('trim', $value);
    $value = array_filter($value);
    
    return $value;
  }
}