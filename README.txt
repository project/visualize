
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Author of Drupal module:
* Javier Reartes (javierreartes)

Co-maintainer
* Mathew Winstone (minorOffense)

Author of jQuery Visualize Plugin:
* see: http://www.filamentgroup.com/lab/jquery_visualize_plugin_accessible_charts_graphs_from_tables_html5_canvas/

jQuery visualize module implements the jQuery Visualize plugin in order to be used with Views

Please note that jQuery Visualize plugin is not related in any way with the author of this module. 
You can find a description of this plugin, documentation about how to use it as well as licence information at their website (see above)


INSTALLATION
------------

1. Download the Visualize library from [https://github.com/filamentgroup/jQuery-Visualize](https://github.com/filamentgroup/jQuery-Visualize)
2. Rename the directory to 'jquery-visualize' and place the folder in a library folder (ex: sites/all/libraries)
3. Enable the module.
4. Call "visualize_add()" or use drupal_add_library('visualize', 'visualize') to include the library.

Usage
-----

@TODO

Configuring Visualize to create customized charts

The following options are available for configuring the type of chart and its visual characteristics:

- type: string. Accepts 'bar', 'area', 'pie', 'line'. Default: 'bar'.
- width: number. Width of chart. Defaults to table width
- height: number. Height of chart. Defaults to table height
- appendTitle: boolean. Add title to chart. Default: true.
- title: string. Title for chart. Defaults to text of table's Caption element.
- appendKey: boolean. Add the color key to the chart. Default: true.
- colors: array. Array items are hex values, used in order of appearance. Default: ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744']
- textColors: array. Array items are hex values. Each item corresponds with colors array. null/undefined items will fall back to CSS text color. Default: [].
- parseDirection: string. Direction to parse the table data. Accepts 'x' and 'y'. Default: 'x'.
- pieMargin: number. Space around outer circle of Pie chart. Default: 20.
- pieLabelPos: string. Position of text labels in Pie chart. Accepts 'inside' and 'outside'. Default: 'inside'.
- lineWeight: number. Stroke weight for lines in line and area charts. Default: 4.
- barGroupMargin: number. Space around each group of bars in a bar chart. Default: 10.
- barMargin: number. Creates space around bars in bar chart (added to both sides of each bar). Default: 1

For more information, see [http://filamentgroup.com/lab/update_to_jquery_visualize_accessible_charts_with_html5_from_designing_with/](http://filamentgroup.com/lab/update_to_jquery_visualize_accessible_charts_with_html5_from_designing_with/)