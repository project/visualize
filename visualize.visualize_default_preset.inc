<?php

/**
 * Implememnts hook_visualize_default_presets().
 */
function visualize_visualize_default_presets() {
  $presets = array();

  $preset = visualize_optionset_create();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'default';
  $preset->optionset_title = 'Default';
  $presets['default'] = $preset;

  return $presets;
}