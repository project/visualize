(function ($) {

// Behavior to load Visualize
Drupal.behaviors.visualize = {
  attach: function(context, settings) {
    for (selector in settings.visualize.instances) {
      $(selector, context).once('visualize', function() {
        if (settings.visualize.optionsets[settings.visualize.instances[selector]] !== undefined) {
          
          var optionset = settings.visualize.optionsets[settings.visualize.instances[selector]];
          if (optionset) {
            $(this).visualize(optionset);
          }
          else {
            $(this).visualize();
          }
        }
      });
    }
  }
};

}(jQuery));